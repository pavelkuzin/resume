Кузин Павел Владимирович
========================
живу в г. Иваново, родился 05.01.1984

- telegram: [@PavelKuzin](https://t.me/PavelKuzin) - предпочитаемый
- [+7 910 999 2412](tel:+79109992412)
- [pavelkuzin@gmail.com](email:pavelkuzin@gmail.com)


## Должность
- специализации: Разработчик, DevOps
- занятость: Полная / Удаленная работа


## Технологии


## Опыт работы


| 2017.ноя - сейчас                         | oboi.ru - руководитель разработки ПО                                                                                                                                                                                                |
|-------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Основное                                  |                                                                                                                                                                                                                                     |
| CI/CD                                     | Отвечаю за полный цикл CI/CD. Bare-metal Kubernetes на Proxmox<br>**Gitlab**, **Gitlab Runner** **Kubernetes**, **Traefik**, **Haproxy**                                                                                            |
| [SaaS-платформа SUP](https://sup.oboi.ru) | Платформа управляет сетью розничных магазинов. Планы, выполнение, анализ продаж, конверсия, база знаний<br>**Python**, **Flask**, **MongoDB**, **Vue**, **Vuetify**, **WebAssembly**, 1С Odata, JWT auth                            |
| [api розничная](https://api.oboi.ru)      | Купоны на скидку с СМС доставкой, актуальные остатки, база знаний, каталог номенклатуры, цены, отбор FIAS<br>**Python**, **Flask**, **MongoDB**, **Vue**, **Vuetify**, 1C Odata, Selectel, Dadata, Unisender                        |
| api оптовая                               | На основе API построены online прайс листы, личный кабинет, мониторинг выполнения планов, десятки мелких сервисов<br>**Python**, **Flask**, **MongoDB**, **ImageMagick**, 1C Odata, api AmoCRM, Selectel CDN, Dadata, Google Maps   |
| 1С Odata proxy                            | Убирает 1С REST API за прокси, агрегирует, обрезает, упрощает работу с дополнительными реквизитами<br>**Python**, **Flask**                                                                                                         |
| document scanner                          | Сканирует пачку первичной документации и договоров, проверяет по сумме, дате, типу документа. Прикрепляет в 1С сущность с отметкой получения документа<br>**Python**, **Flask**, **MongoDB**, **Vue**, **Vuetify**, **OpenCV**      |
| склад - Честный знак                      | Функционал на телефоне: проверить в ЧЗ, отгрузить (ЧЗ добавляется в 1С), принять (проверка в СБИС), инветранизация по штрихкоду<br>**Python**, **Flask**, **Vue**, **Vuetify**, **CryptoPro**, 1C Odata, api Честный Знак, api СБИС |
| дашборды                                  | Описание<br>**Python**, **Clickhouse**, **Datalens**, 1C Odata                                                                                                                                                                      |
|                                           | <br><br><br>                                                                                                                                                                                                                        |
| 2011.ноя - 2017.янв                       | mnogooboev.ru - руководитель ИТ отдела                                                                                                                                                                                              |
|                                           | Построил отдел. Занимался созданием и поддержкой инфраструктуры, бюджетом, автоматизацией процессов<br>Люблю понимать как работает бизнес, за это бизнес делегировал возможность предлагать и настраивать бизнес процессы           |
|                                           | **Linux**, **Bash**, **1C** 7->8.3, **Postfix**, **Zimbra**, **MS Terminal Server**, **OpenVPN**,  **Google Macros**, **OpenCV**, API (Google Yandex Dadata) etc                                                                    |
|                                           | <br>                                                                                                                                                                                                                                |
| 2009.ноя - 2011.ноя                       | **ИП Аутсорсинг**                                                                                                                                                                                                                   |
|                                           | Linux, Windows, ЛВС, Удаленный доступ, Гарантия отказоустойчивости                                                                                                                                                                  |
|                                           | <br>                                                                                                                                                                                                                                |
| 2007.ноя - 2009.июл                       | филиал ФГУ Центр лабораторного анализа и технических измерений по ЦФО                                                                                                                                                               |
|                                           | - ведущий инженер отдела разработки прокетной документации                                                                                                                                                                          |
|                                           | - начальник производственно технического отдела                                                                                                                                                                                     |
|                                           |                                                                                                                                                                                                                                     |
|                                           | Разрабатывал и внедрял ПО для                                                                                                                                                                                                       |
|                                           | учета паспортов опасных отходов и экологических расчетов                                                                                                                                                                            |
|                                           | Руководил ИТ отделом из 2х человек. Ростехнадзор И ЦЛАТИ по Ивановской обл                                                                                                                                                          |
|                                           | <br>                                                                                                                                                                                                                                |
| 2004.июн - 2007.ноя                       | ИП Аутсорсинг                                                                                                                                                                                                                       |
|                                           | - Windows, Linux, ЛВС                                                                                                                                                                                                               |
|                                           |                                                                                                                                                                                                                                     |
| 2005                                      | Ивановский государственный химико-технологический университет                                                                                                                                                                       |
|                                           | - стандартизация и сертификация продуктов химических производств                                                                                                                                                                    |
